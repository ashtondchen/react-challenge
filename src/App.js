import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import fetchJsonp from "fetch-jsonp";

class App extends Component {
  DEFAULT_POSTAL_CODE = 'T2Z4M8';
  API_URL = 'https://represent.opennorth.ca/postcodes/';

  constructor(props) {
    super(props);
    this.state = {
      data: {
        postalCode: this.DEFAULT_POSTAL_CODE,
        representatives_centroid: [],
      }
    }

    this.getMPData = this.getMPData.bind(this);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">
          Enter your postal code to find your MP!
          <br/>
          <input
            minlength="6"
            maxlength="6"
            placeholder={this.state.data.postalCode}
            onChange={this.saveInput}
          />
          <br/>
          <button onClick={this.getMPData}>Go</button>
          {/* Feel free to add styles to App.css! */}
        </p>

        {this.state.data.representatives_centroid.length > 0 ? <div><h3> Representatives Centroid </h3><div className="list">
          {this.state.data.representatives_centroid.map(representative => <div className="list-item"><img className="thumbnail" src={representative.photo_url}></img> <br/>
            {representative.name} <br/>
          {representative.district_name} <br/> {representative.email} </div>)}</div></div> : null}
      </div>
    );
  }

  saveInput = ({target}) => {
    this.setState({
      data: {
        zip: target.value,
        candidates_centroid: [],
        representatives_centroid: [],
        boundaries_centroid: []
      }
    });
    console.log('set zip', target.value)
  };

  getMPData() {
    console.log('Click happened');
    const scope = this
    fetchJsonp(this.API_URL + this.state.data.postalCode)
      .then(function (response) {
        return response.json()
      }).then(function (json) {
      console.log('parsed json', json)
      scope.setState({
        data: json
      });
    }).catch(function (ex) {
      console.log('parsing failed', ex)
    })
  }
}

export default App;
